/* copyright (c) 2022 - 2023 grunfink et al. / MIT license */

/* Permanent Key/Value storage */

/* can be used without the rest of xs */

#ifndef _XS_PKV_H

#define _XS_PKV_H

char *xs_pkv_open(char *fname, int max_size);
void xs_pkv_close(char *f);
void xs_pkv_flush(char *f);
void xs_pkv_set(char *f, char *key, char *value);
char *xs_pkv_get(char *f, char *key);
int xs_pkv_del(char *f, char *key);

void xs_pkv_traverse(char *f, void (*func)(char *key, char *value, void *ctxt), void *ctxt);


#ifdef XS_IMPLEMENTATION

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <stddef.h>
#include <stdint.h>

/* on-disk signature */
#define PKV_SIG 0x0001564b50025358

/* offsets are intentionally 32 bits,
   as a bigger size wouldn't be manageable using this.
   Must be signed because deleted value offsets are negative
   (for the allocated space to be reused in case of undeletion) */

typedef struct pkv {
    int key;              /* offset to key */
    int value;            /* offset to value */
    int child[4];         /* offset to pkv children */
} pkv;

typedef struct pkv_hdr {
    uint64_t sig;         /* signature */
    int first;            /* offset to first node */
    int fd;               /* yes the fd ends up on disk SO WHAT */
} pkv_hdr;


static unsigned int xs_pkv_hash_func(char *p, int size)
/* a general purpose hashing function */
{
    char *e = p + size;
    unsigned int hash = 0x100;

    while (p < e) {
        hash ^= *p++;
        hash *= 111111111;
    }

    return hash ^ hash >> 16;
}


static int xs_pkv_alloc(char *p, void *data, int sz)
/* allocates data to the end of a pkv */
{
    int fd = ((pkv_hdr *)p)->fd;
    int o;

    /* move to the end and get the file size */
    o = lseek(fd, 0, SEEK_END);

    /* truncate to open more room */
    ftruncate(fd, o + sz);

    /* if there is data, copy it there */
    if (data)
        memcpy(p + o, data, sz);

    return o;
}


char *xs_pkv_open(char *fname, int max_size)
/* opens a pkv */
{
    int fd;

    /* set a default size */
    if (max_size == 0)
        max_size = 1024 * 1024 * 1024;

    if ((fd = open(fname, O_RDWR)) == -1) {
        /* cannot open; create it */
        if ((fd = open(fname, O_RDWR | O_CREAT, 0644)) == -1)
            return NULL;

        write(fd, &(pkv_hdr){ PKV_SIG }, sizeof(pkv_hdr));
    }
    else {
        pkv_hdr h;

        if (read(fd, &h, sizeof(h)) != sizeof(h) || h.sig != PKV_SIG) {
            /* truncated file or not a pkv */
            close(fd);
            return NULL;
        }
    }

    char *p = mmap(NULL, max_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (p == MAP_FAILED) {
        close(fd);
        return NULL;
    }

    ((pkv_hdr *)p)->fd = fd;

    return p;
}


void xs_pkv_close(char *f)
/* closes a pkv */
{
    int fd = ((pkv_hdr *)f)->fd;

    /* get the total size of the mapped area */
    int o = lseek(fd, 0, SEEK_END);

    munmap(f, o);
    close(fd);
}


void xs_pkv_flush(char *f)
/* flushes changes to disk */
{
    int fd = ((pkv_hdr *)f)->fd;

    /* get the total size of the mapped area */
    int o = lseek(fd, 0, SEEK_END);

    msync(f, o, MS_SYNC);
}


static int *xs_pkv_locate(char *f, int *off, char *key, int *found)
/* locates a node by key */
{
    unsigned int h = xs_pkv_hash_func(key, strlen(key));
    *found = 0;

    while (*off) {
        /* get the node */
        pkv *c = (pkv *)(f + *off);

        /* get the key */
        char *ckey = f + c->key;

        if (strcmp(key, ckey) == 0) {
            *found = 1;
            break;
        }

        off = &c->child[h >> 30];
        h <<= 2;
    }

    return off;
}


void xs_pkv_set(char *f, char *key, char *value)
/* sets a value */
{
    int found;
    int *o = xs_pkv_locate(f, &((pkv_hdr *)f)->first, key, &found);
    size_t z = strlen(value);

    if (found) {
        /* found! get the node */
        pkv *c = (pkv *)(f + *o);

        /* if the key was deleted, recover the previous value offset */
        if (c->value < 0)
            c->value *= -1;

        char *c_value = f + c->value;

        /* will the new value fit over the previous value? */
        if (strlen(c_value) >= z) {
            /* yes; just copy over it */
            memcpy(c_value, value, z + 1);
        }
        else {
            /* wouldn't fit; allocate the new value
               (previous value is leaked and lost) */
            int v_off = xs_pkv_alloc(f, value, z + 1);
            c->value = v_off;
        }
    }
    else {
        /* not found; add everything */
        int k_off = xs_pkv_alloc(f, key, strlen(key) + 1);
        int v_off = xs_pkv_alloc(f, value, z + 1);

        *o = xs_pkv_alloc(f, &(pkv){ k_off, v_off }, sizeof(pkv));
    }
}


char *xs_pkv_get(char *f, char *key)
/* gets a value */
{
    int found;
    int *o = xs_pkv_locate(f, &((pkv_hdr *)f)->first, key, &found);

    if (found) {
        pkv *c = (pkv *)(f + *o);

        /* return the value only if it's note deleted (negative offset) */
        if (c->value > 0)
            return f + c->value;
    }

    return NULL;
}


int xs_pkv_del(char *f, char *key)
/* deletes a value */
{
    int found;
    int *o = xs_pkv_locate(f, &((pkv_hdr *)f)->first, key, &found);

    if (found) {
        pkv *c = (pkv *)(f + *o);

        /* if c->value is not already deleted, set it to a 'tombstone' (negated value) */
        if (c->value > 0)
            c->value *= -1;
        else
            found = 0;
    }

    return found;
}


static void _pkv_traverse(char *f, int o,
            void (*func)(char *key, char *value, void *ctxt), void *ctxt)
/* calls func with this node's key/value and then its children */
{
    if (o == 0)
        return;

    pkv *c = (pkv *)(f + o);

    if (c->value > 0)
        func(f + c->key, f + c->value, ctxt);

    _pkv_traverse(f, c->child[0], func, ctxt);
    _pkv_traverse(f, c->child[1], func, ctxt);
    _pkv_traverse(f, c->child[2], func, ctxt);
    _pkv_traverse(f, c->child[3], func, ctxt);
}


void xs_pkv_traverse(char *f, void (*func)(char *key, char *value, void *ctxt), void *ctxt)
/* calls func for every (non-deleted) key/value pair */
{
    _pkv_traverse(f, ((pkv_hdr *)f)->first, func, ctxt);
}


#endif /* XS_IMPLEMENTATION */

#endif /* XS_PKV_H */
